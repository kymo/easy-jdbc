package cn.coder.jdbc;

import java.sql.Connection;

/**
 * 数据库事务处理对象
 * 
 * @author YYDF
 *
 */
public interface SqlTranction {

	Connection Connection();

	void commit();

	void rollback(Exception e);

	void close();
}
