package cn.coder.jdbc.support;

import java.sql.SQLException;

import cn.coder.jdbc.SqlSession;
import cn.coder.jdbc.SqlSessionFactory;
import cn.coder.jdbc.SqlTranction;

public abstract class DaoSupport {

	private SqlSessionFactory sessionFactory;

	/**
	 * Spring对sessionFactory赋值接口
	 * 
	 * @param sessionFactory
	 */
	public void setSessionFactory(SqlSessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected SqlSession jdbc() {
		if (this.sessionFactory != null)
			return this.sessionFactory.getSession();
		return SqlSessionFactory.getInstance().getSession();
	}

	protected SqlSession jdbc(String source) {
		if (this.sessionFactory != null)
			return this.sessionFactory.getSession(source);
		return SqlSessionFactory.getInstance().getSession(source);
	}

	protected boolean tran(Run run) {
		return tran(null, run);
	}

	public boolean exist(Object obj) {
		return jdbc().exist(obj);
	}

	public boolean insert(Object obj) {
		return jdbc().insert(obj);
	}

	public boolean update(Object obj) {
		return jdbc().update(obj);
	}

	public boolean delete(Object obj) {
		return jdbc().delete(obj);
	}

	protected boolean tran(String source, Run run) {
		final SqlSession session = (source == null) ? jdbc() : jdbc(source);
		SqlTranction tran = null;
		try {
			tran = session.beginTranction();
			run.exec(session);
			tran.commit();
			return true;
		} catch (Exception e) {
			if (tran != null)
				tran.rollback(e);
			return false;
		}
	}

	protected interface Run {
		// 执行事务
		void exec(final SqlSession session) throws SQLException;
	}
}
