package cn.coder.jdbc.session;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.coder.jdbc.SqlSession;

/**
 * SqlSession动态代理
 * 
 * @author YYDF
 *
 */
public final class ProxySession implements InvocationHandler {
	private static final Logger logger = LoggerFactory.getLogger(ProxySession.class);

	private SqlSession target;

	private ProxySession(SqlSession session) {
		this.target = session;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		logger.debug("Executor: " + method.getName());
		Object result = method.invoke(this.target, args);
		logger.debug("Result:" + result);
		return result;
	}

	public static SqlSession bind(final SqlSession session) {
		return (SqlSession) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
				new Class<?>[] { SqlSession.class }, new ProxySession(session));
	}

}
